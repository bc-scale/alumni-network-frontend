export enum DataStatus {
	Idle,
	Loading,
	Succeeded,
	Failed,
}

export type SnackBarSeverity = "error" | "success" | "info" | "warning";

export enum SnackBarSeverityEnum {
	error = "error",
	success = "success",
	info = "info",
	warning = "warning",
}
