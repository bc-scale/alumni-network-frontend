import { format } from "date-fns";

/**
 * Converts a string to date and formats it
 * @param dateString a string that contains a date
 * @returns a formatted date
 */
export const convertDate = (dateString: string) => {
	const date = new Date(dateString);
	const formattedDate = format(date, "MM-dd-yyyy HH:mm");
	return formattedDate;
};

// OLD METHOD
/**
 * Converts a string to a date. For formating the lastupdated string provided by the backend
 * @param date a date as a string
 * @returns a date in format mm/dd/yyy hh:mm
 */
/*export const convertDate = (dateString: string) => {
	const extraHours = 2;
	const newDate = new Date(dateString);
	return (
		newDate.getMonth() +
		"/" +
		newDate.getDate() +
		"/" +
		newDate.getFullYear() +
		" " +
		(newDate.getHours() + extraHours) +
		":" +
		newDate.getMinutes()
	);
};*/
