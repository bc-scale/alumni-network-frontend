import { debounce, IconButton, InputBase, Paper } from "@mui/material";
import React, { useCallback, useState } from "react";
import SearchIcon from "@mui/icons-material/Search";
import { useAppDispatch } from "../app/hooks";
import { setTimelineSearchQuery } from "./searchQuerySlice";

const SearchBar = () => {
	const dispatch = useAppDispatch();
	const [searchQuery, setSearchQuery] = useState("");

	/** Delay search queries */
	const delayedQuery = useCallback(
		debounce(
			(searchQuery: string) => dispatch(setTimelineSearchQuery(searchQuery)),
			300
		),
		[]
	);

	/**
	 * Updates state to the value of the text in the search bar
	 * @param event
	 */
	const handleSearchChange = (event: any) => {
		setSearchQuery(event.target.value);
		delayedQuery(event.target.value);
	};

	return (
		<Paper
			sx={{ p: "2px 4px", display: "flex", alignItems: "center", width: 400 }}>
			<InputBase
				sx={{ ml: 1, flex: 1 }}
				placeholder="Search for post or event..."
				inputProps={{ "aria-label": "search for post or event" }}
				onChange={handleSearchChange}
				value={searchQuery}
			/>
			<IconButton sx={{ p: "10px" }} aria-label="search">
				<SearchIcon />
			</IconButton>
		</Paper>
	);
};

export default SearchBar;
