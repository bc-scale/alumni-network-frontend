import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../../app/store";

export interface LoginState {
	userName: string;
	password: string;
	loginMessage: string;
}

const initialState: LoginState = {
	userName: "",
	password: "",
	loginMessage: "",
};

/**
 * Part of the redux that handles storing login data
 * Currently only using the loginMessage
 */
export const loginSlice = createSlice({
	name: "login",
	initialState,
	reducers: {
		setUserName: (state, action: PayloadAction<string>) => {
			state.userName = action.payload;
		},
		setPassword: (state, action: PayloadAction<string>) => {
			state.password = action.payload;
		},
		setLoginMessage: (state, action: PayloadAction<string>) => {
			state.loginMessage = action.payload;
		},
	},
	extraReducers: (builder) => {},
});

export const { setUserName, setPassword, setLoginMessage } = loginSlice.actions;

// Selectors
export const selectUserName = (state: RootState) => state.login.userName;
export const selectPassword = (state: RootState) => state.login.password;
export const selectLoginMessage = (state: RootState) =>
	state.login.loginMessage;

export default loginSlice.reducer;
