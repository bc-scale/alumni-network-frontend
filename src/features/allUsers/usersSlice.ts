import { DataStatus } from "./../../common/commonTypes";
import { IUser, IUsersById } from "./usersTypes";
import { fetchUsers } from "./../../api/user/userApi";
import { RootState } from "./../../app/store";
import { createAsyncThunk, createSlice, PayloadAction } from "@reduxjs/toolkit";

export const fetchAllUsers = createAsyncThunk(
	"users/fetchUsers",
	async (_, thunkAPI) => {
		const response = await fetchUsers();
		if (!response.success) {
			return thunkAPI.rejectWithValue({
				errorMessage: "Could not fetch users",
			});
		}
		return response.data;
	}
);

export interface IUsersState {
	usersStatus: DataStatus;
	allUsersIds: string[];
	userById: {
		[id: number]: IUser;
	};
	currentUserId?: string;
}

const initialState: IUsersState = {
	usersStatus: DataStatus.Idle,
	allUsersIds: [],
	userById: {},
};

/**
 * Part of the redux store that handles storing all users
 * Following the Redux normalizing pattern: {@link https://redux.js.org/usage/structuring-reducers/normalizing-state-shape#designing-a-normalized-state Redux normalizing state}
 */
export const allUsersSlice = createSlice({
	name: "allUsers",
	initialState,
	reducers: {
		setCurrentUserId: (state, action: PayloadAction<string | undefined>) => {
			if (action.payload !== undefined) state.currentUserId = action.payload;
			else state.currentUserId = undefined;
		},
		setAllUsersStatus: (state, action: PayloadAction<DataStatus>) => {
			state.usersStatus = action.payload;
		},
	},

	extraReducers: (builder) => {
		builder.addCase(fetchAllUsers.fulfilled, (state, action) => {
			const byId = action.payload.reduce((byId: IUsersById, user: IUser) => {
				byId[user.userId] = user;
				return byId;
			}, {});
			state.userById = byId;
			state.allUsersIds = Object.keys(byId);
			state.usersStatus = DataStatus.Succeeded;
		});
	},
});

export const { setCurrentUserId, setAllUsersStatus } = allUsersSlice.actions;

// Selectors
export const selectAllUserIds = (state: RootState) =>
	state.allUsers.allUsersIds;
export const selectUserById = (state: RootState, id: number) =>
	state.allUsers.userById[id];
export const selectUserStatus = (state: RootState) =>
	state.allUsers.usersStatus;
export const selectCurrentUserId = (state: RootState) =>
	state.allUsers.currentUserId;

export default allUsersSlice.reducer;
