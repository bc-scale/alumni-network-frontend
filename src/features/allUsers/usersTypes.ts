export interface IUsersById {
	[id: number]: IUser;
}

export interface IUser {
	userId: number;
	name: string;
	picture: string;
	status: string;
	bio: string;
	funFact: string;
	groups?: number[];
	topics?: number[];
	events?: number[];
	posts?: number[];
	rsvps?: number[];
}
