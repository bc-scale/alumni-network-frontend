import {
	Divider,
	Grid,
	List,
	ListSubheader,
	Popover,
	Typography,
} from "@mui/material";
import React from "react";
import { useAppSelector } from "../../app/hooks";
import { NavbarListProps } from "../group/GroupList";
import TopicListItem from "./TopicListItem";
import { selectTopicIds } from "./topicSlice";

/**
 * The topic popover list menu component
 * @param props
 * @returns The topic list
 */
const TopicList = (props: NavbarListProps) => {
	const topicIds = useAppSelector(selectTopicIds);
	return (
		<Popover
			open={props.open}
			onClose={props.handleClose}
			anchorEl={props.anchorEl}
			anchorOrigin={{
				vertical: "bottom",
				horizontal: "left",
			}}>
			<Grid
				container
				sx={{ backgroundColor: "" }}
				direction="column"
				padding={1}>
				<List subheader={<ListSubheader>Topics</ListSubheader>}>
					<Divider />
					{topicIds.length > 0
						? topicIds.map((id) => <TopicListItem id={id} key={id} />)
						: "No topics"}
				</List>
			</Grid>
		</Popover>
	);
};

export default TopicList;
