import { ITopicMembershipRequest } from "./../../api/topic/topicTypes";
import { RootState } from "./../../app/store";
import { ITopic, ITopicById } from "./topicTypes";
import {
	createTopicMembership,
	deleteTopicMembership,
	fetchTopics,
} from "./../../api/topic/topicApi";
import { createAsyncThunk, createSlice, PayloadAction } from "@reduxjs/toolkit";
import { DataStatus } from "../../common/commonTypes";
import { IGroupById } from "../group/groupTypes";

export const fetchAllTopics = createAsyncThunk(
	"topic/fetchTopics",
	async (_, thunkAPI) => {
		const response = await fetchTopics();
		if (!response.success) {
			return thunkAPI.rejectWithValue({
				errorMessage: "Could not fetch topics",
			});
		}
		return response.data;
	}
);

export const subscribeToTopicRequest = createAsyncThunk(
	"topic/subscribe",
	async (req: ITopicMembershipRequest, thunkAPI) => {
		const response = await createTopicMembership(req);
		if (!response.success) {
			return thunkAPI.rejectWithValue({
				errorMessage: "Could not create topic membership",
			});
		}
		return response.data;
	}
);

export const unsubscribeToTopic = createAsyncThunk(
	"topic/unsubscribe",
	async (req: ITopicMembershipRequest, thunkAPI) => {
		const response = await deleteTopicMembership(req);
		if (!response.success) {
			return thunkAPI.rejectWithValue({
				errorMessage: "Could not unsubscribe",
			});
		}
		return response.data;
	}
);

interface TopicState {
	topicStatus: DataStatus;
	topicIds: string[];
	topicById: IGroupById;
	activeTopic?: string;
}

const initialState: TopicState = {
	topicStatus: DataStatus.Idle,
	topicIds: [],
	topicById: {},
};

/**
 * Part of the redux store that handles storing all topics
 * Following the Redux normalizing pattern: {@link https://redux.js.org/usage/structuring-reducers/normalizing-state-shape#designing-a-normalized-state Redux normalizing state}
 */
export const topicSlice = createSlice({
	name: "topic",
	initialState,
	reducers: {
		setActiveTopicId: (state, action: PayloadAction<string | undefined>) => {
			if (action.payload !== undefined)
				if (state.activeTopic === action.payload) state.activeTopic = undefined;
				else state.activeTopic = action.payload;
			else state.activeTopic = undefined;
		},
	},
	extraReducers: (builder) => {
		builder.addCase(fetchAllTopics.fulfilled, (state, action) => {
			const byId = action.payload.reduce((byId: ITopicById, topic: ITopic) => {
				byId[topic.topicId] = topic;
				return byId;
			}, {});
			state.topicById = byId;
			state.topicIds = Object.keys(byId);
			state.topicStatus = DataStatus.Succeeded;
		});
	},
});

export const { setActiveTopicId } = topicSlice.actions;

// Selectors
export const selectTopicIds = (state: RootState) => state.topic.topicIds;
export const selectTopicById = (state: RootState) => state.topic.topicById;
export const selectTopicStatus = (state: RootState) => state.topic.topicStatus;
export const selectActiveTopicId = (state: RootState) =>
	state.topic.activeTopic;

export default topicSlice.reducer;
