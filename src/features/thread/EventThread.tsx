import {
	Avatar,
	Button,
	ButtonBase,
	Divider,
	Grid,
	IconButton,
	Paper,
	Tooltip,
	Typography,
} from "@mui/material";
import React, { useState } from "react";
import { IPostBody } from "../../api/post/postApiTypes";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { convertDate } from "../../common/helperFunctions";
import { selectGroupById } from "../group/groupSlice";
import { createPost, selectPostById, selectPostsIds } from "../post/postSlice";
import { selectUserId } from "../user/userSlice";
import PostThreadItem from "./PostThreadItem";
import ReplyPost from "./ReplyPost";
import CloseIcon from "@mui/icons-material/Close";
import { setActiveEventId } from "../event/eventSlice";
import EventIcon from "@mui/icons-material/Event";

interface ThreadProps {
	id: string;
	handleUsernameClick: (userId: string) => void;
}

/**
 * The event thread component
 * @param props
 * @returns the event thread component
 */
const EventThread = (props: ThreadProps) => {
	const dispatch = useAppDispatch();

	const [success, setSuccess] = useState(false);
	const [error, setError] = useState<string | undefined>();

	const event = useAppSelector(
		(state) => state.event.eventById[parseInt(props.id)]
	);
	// get the owner of the event
	const owner = useAppSelector(
		(state) => state.allUsers.userById[event.createdBy]
	);
	const groupById = useAppSelector(selectGroupById);
	const loggedInUserId = useAppSelector(selectUserId);
	const postIds = useAppSelector(selectPostsIds);
	const post = useAppSelector(selectPostById);

	const usernameClick = () => {
		props.handleUsernameClick(owner.userId.toString());
	};

	const handleSendMessageClick = (e: any, message: string) => {
		e.preventDefault();
		setSuccess(false);
		if (loggedInUserId !== 0 && event) {
			const post: IPostBody = {
				lastUpdated: "",
				postMessage: message,
				title: "title",
				senderId: loggedInUserId,
				target_event: {
					eventId: event.eventId,
				},
			};
			console.log(post);
			dispatch(createPost(post))
				.unwrap()
				.then(() => {
					setSuccess(true);
				})
				.catch((error) => {
					console.log(error);
					setError(error.errorMessage);
				});
		}
	};

	const handleCloseThread = () => {
		dispatch(setActiveEventId(undefined));
	};

	return (
		<Grid item sx={{ mt: 9.5 }}>
			<Grid item>
				<Paper elevation={5} sx={{ padding: 2 }}>
					<Grid container alignItems="center" justifyContent="space-between">
						<Grid item>
							<Grid container alignItems="center" spacing={2}>
								<Grid item>
									<Grid container alignItems="center">
										<EventIcon sx={{ mr: 1 }} />
										<Typography component="h1" variant="h4">
											{event.title}
										</Typography>
									</Grid>
								</Grid>
								<Grid item>
									<Typography component="div" variant="body2">
										{event.groups && event.groups?.length > 0
											? event.groups.map((id) => "#" + groupById[id].name)
											: "#No group"}
									</Typography>
								</Grid>
							</Grid>
						</Grid>
						<Grid item>
							<IconButton onClick={handleCloseThread}>
								<CloseIcon />
							</IconButton>
						</Grid>
					</Grid>
					<Divider sx={{ mb: 2 }} />
					<Grid
						container
						justifyContent="start"
						alignItems="center"
						spacing={2}>
						<Grid item>
							<Avatar src={owner.picture} alt="" />
						</Grid>
						<Grid item>
							<Grid container alignItems="center">
								<Grid item>
									<Tooltip
										placement="top"
										title={"Go to " + owner.name + "'s profile"}>
										<Typography
											onClick={usernameClick}
											component="h1"
											variant="h5"
											sx={{
												mr: 2,
												padding: 0.5,
												borderRadius: 4,
												"&:hover": { cursor: "pointer", bgcolor: "lavender" },
											}}>
											{owner.name}
										</Typography>
									</Tooltip>
								</Grid>

								<Grid item>
									<Typography variant="subtitle2">
										<span style={{ fontWeight: "bold" }}>Start: </span>
										{convertDate(event.startTime)}
									</Typography>
									<Typography variant="subtitle2">
										<span style={{ fontWeight: "bold" }}>End: </span>
										{convertDate(event.endTime)}
									</Typography>
								</Grid>
							</Grid>
						</Grid>
					</Grid>
					<Grid item sx={{ ml: 7.5 }}>
						<Typography component="h1" variant="h6">
							{event.description}
						</Typography>
					</Grid>
					{postIds.map(
						(id: string) =>
							post[parseInt(id)].target_event === parseInt(props.id) && (
								<PostThreadItem
									key={id}
									id={parseInt(id)}
									threadId={props.id}
									handleUsernameClick={props.handleUsernameClick}
								/>
							)
					)}
					<Grid container marginTop={2}>
						<ReplyPost
							handleSendMessageClick={handleSendMessageClick}
							success={success}
							error={error}
						/>
					</Grid>
				</Paper>
			</Grid>
		</Grid>
	);
};

export default EventThread;
