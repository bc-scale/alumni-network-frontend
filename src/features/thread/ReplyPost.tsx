import {
	Grid,
	Box,
	FormControl,
	OutlinedInput,
	InputAdornment,
	IconButton,
	Typography,
} from "@mui/material";
import React, { useEffect, useState } from "react";
import SendRoundedIcon from "@mui/icons-material/SendRounded";

interface IReplyPostProps {
	handleSendMessageClick: (event: any, message: string) => void;
	success: boolean;
	error?: string;
}

/**
 * The reply message component (reply box) at the bottom post/event thread
 * @param props
 * @returns reply message component
 */
const ReplyPost = (props: IReplyPostProps) => {
	const [message, setMessage] = useState("");

	const handleMessageChange = (event: React.ChangeEvent<HTMLInputElement>) => {
		setMessage(event.target.value);
	};

	useEffect(() => {
		if (props.success) setMessage("");
	}, [props.success]);

	return (
		<Grid container>
			<Box
				sx={{ width: 1200, maxWidth: "100%" }}
				component="form"
				noValidate
				autoComplete="off"
				onSubmit={(e: any) => props.handleSendMessageClick(e, message)}>
				<FormControl fullWidth>
					<OutlinedInput
						fullWidth
						onChange={handleMessageChange}
						value={message}
						placeholder="Write your message here..."
						multiline
						endAdornment={
							<InputAdornment position="end">
								<IconButton
									onClick={(e) => props.handleSendMessageClick(e, message)}
									aria-label="Send message"
									edge="end">
									<SendRoundedIcon />
								</IconButton>
							</InputAdornment>
						}></OutlinedInput>
					{props.error && <Typography color="error">{props.error}</Typography>}
				</FormControl>
			</Box>
		</Grid>
	);
};

export default ReplyPost;
