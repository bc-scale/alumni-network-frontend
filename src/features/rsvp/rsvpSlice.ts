import { DataStatus } from "./../../common/commonTypes";
import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { fetchRsvp } from "../../api/rsvp/rsvpApi";
import { IRsvp, IRsvpById } from "./rsvpTypes";

interface RsvpState {
	rsvpStatus: DataStatus;
	rsvpIds: string[];
	rsvpById: IRsvpById;
	activeRsvp?: string;
}

const initialState: RsvpState = {
	rsvpStatus: DataStatus.Idle,
	rsvpIds: [],
	rsvpById: {},
};

export const fetchAllRsvp = createAsyncThunk(
	"rsvp/getRsvp",
	async (_, thunkAPI) => {
		const response = await fetchRsvp();
		if (!response.success) {
			return thunkAPI.rejectWithValue({
				errorMessage: "Error fetching rsvps",
			});
		}
		return response.data;
	}
);

/**
 * Part of the redux store that handles storing all rsvp
 * Following the Redux normalizing pattern: {@link https://redux.js.org/usage/structuring-reducers/normalizing-state-shape#designing-a-normalized-state Redux normalizing state}
 */
export const rsvpSlice = createSlice({
	name: "rsvp",
	initialState,
	reducers: {},
	extraReducers: (builder) => {
		builder.addCase(fetchAllRsvp.fulfilled, (state, action) => {
			const byId = action.payload.reduce((byId: IRsvpById, rsvp: IRsvp) => {
				byId[rsvp.rsvpid] = rsvp;
				return byId;
			}, {});
			state.rsvpById = byId;
			state.rsvpIds = Object.keys(byId);
			state.rsvpStatus = DataStatus.Succeeded;
		});
	},
});

export const {} = rsvpSlice.actions;

export default rsvpSlice.reducer;
