import { LocalizationProvider, DateTimePicker } from "@mui/lab";
import AdapterDateFns from "@mui/lab/AdapterDateFns";
import {
	Modal,
	Backdrop,
	Fade,
	Box,
	Typography,
	TextField,
	Grid,
	Button,
	Alert,
	Snackbar,
	Switch,
	Tooltip,
	Divider,
} from "@mui/material";
import React, { useState } from "react";
import { IEventBody } from "../../api/events/eventTypes";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import {
	selectGroupById,
	selectPublicAndPartOfGroups,
} from "../group/groupSlice";
import { selectTopicById, selectTopicIds } from "../topic/topicSlice";
import { selectUserId } from "../user/userSlice";
import { createEvent } from "./eventSlice";

interface eventProps {
	openEvent: boolean;
	handleCloseEvent: (openEvent: boolean) => void;
}

const EventModal = (props: eventProps) => {
	const dispatch = useAppDispatch();

	// Local states
	const [eventTitle, setEventTitle] = useState<string>("");
	const [allowGuests, setAllowGuests] = useState(true);
	const [eventGroup, setEventGroup] = useState<string>("");
	const [eventTopic, setEventTopic] = useState<string>("");
	const [eventStartDate, setEventStartDate] = useState<Date>(new Date());
	const [eventEndDate, setEventEndDate] = useState<Date>(new Date());
	const [content, setContent] = useState<string>("");
	const [startValue, setStartValue] = useState<Date | null>(new Date());
	const [endValue, setEndValue] = useState<Date | null>(new Date());
	const [openSnack, setOpenSnack] = useState(false);

	// Hooks
	const groups = useAppSelector(selectPublicAndPartOfGroups);
	const groupById = useAppSelector(selectGroupById);
	const topics = useAppSelector(selectTopicIds);
	const topicById = useAppSelector(selectTopicById);
	const creatorUserId = useAppSelector(selectUserId);

	const requiredFieldsFilledCorrect =
		eventTitle.length < 3 || content.length < 3;

	const handleEventTitleChange = (
		event: React.ChangeEvent<HTMLInputElement>
	) => {
		setEventTitle(event.target.value);
	};

	const handleEventGroupChange = (
		event: React.ChangeEvent<HTMLInputElement>
	) => {
		setEventGroup(event.target.value);
	};

	const handleEventTopicChange = (
		event: React.ChangeEvent<HTMLInputElement>
	) => {
		setEventTopic(event.target.value);
	};

	const handleEventStartDateChange = (event: any) => {
		setEventStartDate(event.target.value);
	};

	const handleEventEndDateChange = (event: any) => {
		setEventEndDate(event.target.value);
	};

	const handleContentChange = (event: React.ChangeEvent<HTMLInputElement>) => {
		setContent(event.target.value);
	};

	const cancelClick = () => {
		props.handleCloseEvent(false);
	};

	/**
	 * Handling closing the Modal.
	 * @param event
	 * @param reason the reason for closing
	 * @returns
	 */
	const handleClose = (
		event?: React.SyntheticEvent | Event,
		reason?: string
	) => {
		if (reason === "clickaway") {
			return;
		}
		setOpenSnack(false);
	};

	/**
	 * Creates a new event and closes the modal.
	 * It takes the value of the local states and stores it in a event object
	 * If there are any errors the modal will not close and a error message is displayed in the console.
	 */
	const createEventClick = () => {
		const event: IEventBody = {
			createdBy: creatorUserId,
			title: eventTitle,
			allowGuests,
			description: content,
			startTime: startValue,
			endTime: endValue,
		};
		dispatch(createEvent(event))
			.unwrap()
			.then(() => {
				props.handleCloseEvent(false);
				setOpenSnack(true);
			})
			.catch((error) => {
				console.log(error);
			});
	};

	const handleStartChange = (newStartValue: Date | null) => {
		setStartValue(newStartValue);
	};

	const handleEndChange = (newEndValue: Date | null) => {
		setEndValue(newEndValue);
	};

	const handleAllowGuestsChange = () => {
		setAllowGuests(!allowGuests);
	};

	// Style for the box in the modal
	const style = {
		position: "absolute" as "absolute",
		top: "50%",
		left: "50%",
		transform: "translate(-50%, -50%)",
		width: 600,
		bgcolor: "white",
		boxShadow: 24,
		p: 4,
		borderRadius: 2,
	};

	return (
		<>
			<Modal
				aria-labelledby="transition-modal-title"
				aria-describedby="transition-modal-description"
				open={props.openEvent}
				onClose={props.handleCloseEvent}
				closeAfterTransition
				BackdropComponent={Backdrop}>
				<Fade in={props.openEvent}>
					<Box sx={style}>
						<Typography
							id="transition-modal-title"
							variant="h5"
							textAlign="center">
							Create a new event
						</Typography>
						<Divider variant="middle" />
						<Typography variant="subtitle1" textAlign="center">
							Title and description must be filled out
						</Typography>
						<Grid container>
							<TextField
								margin="normal"
								fullWidth
								required
								id="title"
								label="Title"
								name="Title"
								defaultValue={eventTitle}
								onChange={handleEventTitleChange}
							/>
							<Grid
								container
								justifyContent="space-between"
								alignItems="center">
								<Grid item>
									<Typography>
										Allow guests?
										<span
											style={{
												marginLeft: 4,
												color: allowGuests ? "green" : "red",
											}}>
											{allowGuests ? "Yes" : "No"}
										</span>
									</Typography>
								</Grid>

								<Grid item>
									<Switch
										checked={allowGuests}
										onChange={handleAllowGuestsChange}
									/>
								</Grid>
							</Grid>
							{/*<TextField
								select
								margin="normal"
								fullWidth
								name="group"
								label="Group"
								id="group"
								value={eventGroup}
								onChange={handleEventGroupChange}>
								{groups.map((id) => (
									<MenuItem key={id} value={id}>
										{id}. {groupById[parseInt(id)].name}
									</MenuItem>
								))}
							</TextField>
							<TextField
								select
								margin="normal"
								fullWidth
								name="topic"
								label="Topic"
								id="topic"
								value={eventTopic}
								onChange={handleEventTopicChange}>
								{topics.map((id) => (
									<MenuItem key={id} value={id}>
										{id}. {topicById[parseInt(id)].name}
									</MenuItem>
								))}
							</TextField>*/}
							<LocalizationProvider dateAdapter={AdapterDateFns}>
								<DateTimePicker
									label="Start of Event"
									value={startValue}
									minDate={new Date("2022-03-01")}
									onChange={handleStartChange}
									renderInput={(params) => (
										<TextField
											{...params}
											fullWidth
											value={eventStartDate}
											onChange={handleEventStartDateChange}
											type="datetime-local"
											sx={{ mt: 2 }}
										/>
									)}
								/>
							</LocalizationProvider>
							<LocalizationProvider dateAdapter={AdapterDateFns}>
								<DateTimePicker
									label="End of Event"
									value={endValue}
									minDate={new Date("2022-03-01")}
									onChange={handleEndChange}
									renderInput={(params) => (
										<TextField
											{...params}
											fullWidth
											value={eventEndDate}
											onChange={handleEventEndDateChange}
											type="datetime-local"
											sx={{ mt: 2 }}
										/>
									)}
								/>
							</LocalizationProvider>
							<TextField
								margin="normal"
								fullWidth
								required
								name="description"
								label="Description"
								id="description"
								multiline
								rows={10}
								value={content}
								onChange={handleContentChange}
							/>
							<Grid container justifyContent="space-between" marginTop={2}>
								<Grid item>
									<Button
										color="error"
										variant="contained"
										onClick={cancelClick}>
										Cancel
									</Button>
								</Grid>
								<Grid item>
									<Tooltip
										arrow={true}
										title={
											requiredFieldsFilledCorrect
												? "Title and post content must contain 3 or more letters"
												: "Create Event"
										}>
										<span>
											<Button
												onClick={createEventClick}
												variant="contained"
												disabled={requiredFieldsFilledCorrect}>
												Create
											</Button>
										</span>
									</Tooltip>
								</Grid>
							</Grid>
						</Grid>
					</Box>
				</Fade>
			</Modal>
			<Snackbar open={openSnack} autoHideDuration={6000} onClose={handleClose}>
				<Alert
					onClose={handleClose}
					severity="success"
					sx={{ width: "100%", fontSize: 24, bgcolor: "lightgreen" }}>
					Event successfully Created !
				</Alert>
			</Snackbar>
		</>
	);
};

export default EventModal;
