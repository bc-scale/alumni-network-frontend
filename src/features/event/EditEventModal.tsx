import { LocalizationProvider, DateTimePicker } from "@mui/lab";
import AdapterDateFns from "@mui/lab/AdapterDateFns";
import {
	Modal,
	Backdrop,
	Fade,
	Box,
	Typography,
	TextField,
	Grid,
	Button,
	MenuItem,
	Alert,
	Snackbar,
} from "@mui/material";
import React, { useState } from "react";
import { IEventBody } from "../../api/events/eventTypes";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { selectGroupIds, selectGroupById } from "../group/groupSlice";
import { selectTopicById, selectTopicIds } from "../topic/topicSlice";
import { selectUserId } from "../user/userSlice";
import { createEvent } from "./eventSlice";

interface editEventProps {
	openEditEvent: boolean;
	handleCloseEditEvent: (openPost: boolean) => void;
}

const EditEventModal = (props: editEventProps) => {
	const dispatch = useAppDispatch();

	// Local States
	const [editEventTitle, setEditEventTitle] = useState<string>("");
	const [editEventGroup, setEditEventGroup] = useState<string>("");
	const [editEventTopic, setEditEventTopic] = useState<string>("");
	const [editEventStartDate, setEditEventStartDate] = useState<Date>(
		new Date()
	);
	const [editEventEndDate, setEditEventEndDate] = useState<Date>(new Date());
	const [editContent, setEditContent] = useState<string>("");
	const [editStartValue, setEditStartValue] = useState<Date | null>(new Date());
	const [editEndValue, setEditEndValue] = useState<Date | null>(new Date());
	const [openSnack, setOpenSnack] = useState(false);

	// Hooks
	const groups = useAppSelector(selectGroupIds);
	const groupById = useAppSelector(selectGroupById);
	const topics = useAppSelector(selectTopicIds);
	const topicById = useAppSelector(selectTopicById);
	const creatorUserId = useAppSelector(selectUserId);

	const handleEditEventTitleChange = (
		event: React.ChangeEvent<HTMLInputElement>
	) => {
		setEditEventTitle(event.target.value);
	};

	const handleEditEventGroupChange = (
		event: React.ChangeEvent<HTMLInputElement>
	) => {
		setEditEventGroup(event.target.value);
	};

	const handleEditEventTopicChange = (
		event: React.ChangeEvent<HTMLInputElement>
	) => {
		setEditEventTopic(event.target.value);
	};

	const handleEditEventStartDateChange = (event: any) => {
		setEditEventStartDate(event.target.value);
	};

	const handleEditEventEndDateChange = (event: any) => {
		setEditEventEndDate(event.target.value);
	};

	const handleEditContentChange = (
		event: React.ChangeEvent<HTMLInputElement>
	) => {
		setEditContent(event.target.value);
	};

	const cancelClick = () => {
		props.handleCloseEditEvent(false);
	};

	const handleClose = (
		event?: React.SyntheticEvent | Event,
		reason?: string
	) => {
		if (reason === "clickaway") {
			return;
		}
		setOpenSnack(false);
	};

	const editEventClick = () => {
		const event: IEventBody = {
			createdBy: creatorUserId,
			title: editEventTitle,
			allowGuests: true,
			description: editContent,
			startTime: editStartValue,
			endTime: editEndValue,
		};
		dispatch(createEvent(event));
		props.handleCloseEditEvent(false);
		setOpenSnack(true);
	};

	const handleEditStartChange = (newStartValue: Date | null) => {
		setEditStartValue(newStartValue);
	};

	const handleEditEndChange = (newEndValue: Date | null) => {
		setEditEndValue(newEndValue);
	};

	const style = {
		position: "absolute" as "absolute",
		top: "50%",
		left: "50%",
		transform: "translate(-50%, -50%)",
		width: 600,
		bgcolor: "white",
		border: "2px solid #000",
		boxShadow: 24,
		p: 4,
	};

	return (
		<>
			<Modal
				aria-labelledby="transition-modal-title"
				aria-describedby="transition-modal-description"
				open={props.openEditEvent}
				onClose={props.handleCloseEditEvent}
				closeAfterTransition
				BackdropComponent={Backdrop}>
				<Fade in={props.openEditEvent}>
					<Box sx={style}>
						<Typography
							id="transition-modal-title"
							variant="h6"
							component="h2"
							textAlign="center">
							Edit Event
						</Typography>
						<Typography sx={{ mt: 2 }}>
							<TextField
								margin="normal"
								fullWidth
								id="title"
								label="Title"
								name="Title"
								defaultValue={editEventTitle}
								onChange={handleEditEventTitleChange}
							/>
							<TextField
								select
								margin="normal"
								fullWidth
								name="group"
								label="Group"
								id="group"
								defaultValue={editEventGroup}
								onChange={handleEditEventGroupChange}>
								{groups.map((id) => (
									<MenuItem key={id} value={id}>
										{id}. {groupById[parseInt(id)].name}
									</MenuItem>
								))}
							</TextField>
							<TextField
								select
								margin="normal"
								fullWidth
								name="topic"
								label="Topic"
								id="topic"
								defaultValue={editEventTopic}
								onChange={handleEditEventTopicChange}>
								{topics.map((id) => (
									<MenuItem key={id} value={id}>
										{id}. {topicById[parseInt(id)].name}
									</MenuItem>
								))}
							</TextField>
							<LocalizationProvider dateAdapter={AdapterDateFns}>
								<DateTimePicker
									label="Start of Event"
									value={editStartValue}
									minDate={new Date("2022-03-01")}
									onChange={handleEditStartChange}
									renderInput={(params) => (
										<TextField
											{...params}
											fullWidth
											defaultValue={editEventStartDate}
											onChange={handleEditEventStartDateChange}
											type="datetime-local"
											sx={{ mt: 2 }}
										/>
									)}
								/>
							</LocalizationProvider>
							<LocalizationProvider dateAdapter={AdapterDateFns}>
								<DateTimePicker
									label="End of Event"
									value={editEndValue}
									minDate={new Date("2022-03-01")}
									onChange={handleEditEndChange}
									renderInput={(params) => (
										<TextField
											{...params}
											fullWidth
											defaultValue={editEventEndDate}
											onChange={handleEditEventEndDateChange}
											type="datetime-local"
											sx={{ mt: 2 }}
										/>
									)}
								/>
							</LocalizationProvider>
							<TextField
								margin="normal"
								fullWidth
								name="postContent"
								label="Post Content"
								id="postContent"
								multiline
								rows={16}
								defaultValue={editContent}
								onChange={handleEditContentChange}
							/>
							<Grid container justifyContent="space-between">
								<Button
									color="error"
									variant="contained"
									sx={{ mt: 3, mb: 2 }}
									onClick={cancelClick}>
									Cancel
								</Button>
								<Button
									onClick={editEventClick}
									variant="contained"
									sx={{ mt: 3, mb: 2 }}>
									Save changes
								</Button>
							</Grid>
						</Typography>
					</Box>
				</Fade>
			</Modal>
			<Snackbar open={openSnack} autoHideDuration={6000} onClose={handleClose}>
				<Alert
					onClose={handleClose}
					severity="success"
					sx={{ width: "100%", fontSize: 24, bgcolor: "lightgreen" }}>
					Event successfully Updated !
				</Alert>
			</Snackbar>
		</>
	);
};

export default EditEventModal;
