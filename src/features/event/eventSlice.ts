import { RootState } from "./../../app/store";
import { IEventBody } from "./../../api/events/eventTypes";
import { IEvent, IEventById } from "./eventTypes";
import { DataStatus } from "./../../common/commonTypes";
import { createAsyncThunk, createSlice, PayloadAction } from "@reduxjs/toolkit";
import { createEventRequest, fetchEvents } from "../../api/events/eventApi";

export const fetchAllEvents = createAsyncThunk(
	"event/fetchEvents",
	async (_, thunkAPI) => {
		const response = await fetchEvents();
		if (!response.success) {
			return thunkAPI.rejectWithValue({
				errorMessage: "Could not fetch events",
			});
		}
		return response.data;
	}
);

export const createEvent = createAsyncThunk(
	"event/createEvent",
	async (event: IEventBody, thunkAPI) => {
		const response = await createEventRequest(event);
		if (!response.success) {
			return thunkAPI.rejectWithValue({
				errorMessage: "Could not create Event",
			});
		}
		return response.data;
	}
);

interface EventState {
	eventStatus: DataStatus;
	eventIds: string[];
	eventById: IEventById;
	activeEvent?: string;
}

const initialState: EventState = {
	eventStatus: DataStatus.Idle,
	eventIds: [],
	eventById: {},
};

/**
 * Part of the redux store that handles storing of all events
 * Following the Redux normalizing pattern: {@link https://redux.js.org/usage/structuring-reducers/normalizing-state-shape#designing-a-normalized-state Redux normalizing state}
 */
export const eventSlice = createSlice({
	name: "event",
	initialState,
	reducers: {
		setActiveEventId: (state, action: PayloadAction<string | undefined>) => {
			if (action.payload !== undefined)
				if (state.activeEvent === action.payload) state.activeEvent = undefined;
				else state.activeEvent = action.payload;
			else state.activeEvent = undefined;
		},
		setEventStatus: (state, action: PayloadAction<DataStatus>) => {
			state.eventStatus = action.payload;
		},
	},
	extraReducers: (builder) => {
		builder.addCase(fetchAllEvents.fulfilled, (state, action) => {
			const byId = action.payload.reduce((byId: IEventById, event: IEvent) => {
				byId[event.eventId] = event;
				return byId;
			}, {});
			state.eventById = byId;
			state.eventIds = Object.keys(byId);
			state.eventStatus = DataStatus.Succeeded;
		});
	},
});

export const { setActiveEventId, setEventStatus } = eventSlice.actions;

export const selectEventIds = (state: RootState) => state.event.eventIds;
export const selectEventById = (state: RootState) => state.event.eventById;
export const selectActiveEvent = (state: RootState) => state.event.activeEvent;
export const selectEventStatus = (state: RootState) => state.event.eventStatus;

/**
 * Filtering the events based on a search query and sorts it on date
 * @param state all states in the redux store
 * @returns a filtered and sorted list of events
 */
export const selectFilteredEventIds = (state: RootState) => {
	let filteredEventIds: string[] = [];
	const query = state.search.searchQuery;
	state.event.eventIds.forEach((id) => {
		const event = state.event.eventById[parseInt(id)];
		const creator = state.allUsers.userById[event.createdBy];
		const usersInEvent: string[] = [];
		event.users &&
			event.users?.forEach((userId) => {
				const username = state.allUsers.userById[userId].name;
				usersInEvent.push(username.toLowerCase());
			});
		if (
			event.description.toLowerCase().includes(query.toLowerCase()) ||
			creator.name.toLowerCase().includes(query.toLowerCase()) ||
			event.title.toLowerCase().includes(query.toLowerCase()) ||
			usersInEvent.includes(query.toLowerCase())
		)
			filteredEventIds.push(id);
	});
	sortEventsByLastUpdated(state, filteredEventIds);
	return filteredEventIds;
};

/**
 * Sorts a list of events by date
 * @param state all states in the redux store
 * @param eventIds the eventIds of the events to be sorted
 * @return a sorted list of events
 */
const sortEventsByLastUpdated = (state: RootState, eventIds: string[]) => {
	eventIds.sort((a: string, b: string) => {
		const byId = state.event.eventById;
		const first = byId[parseInt(a)];
		const second = byId[parseInt(b)];
		return (
			new Date(second.lastUpdated).valueOf() -
			new Date(first.lastUpdated).valueOf()
		);
	});
};

export default eventSlice.reducer;
