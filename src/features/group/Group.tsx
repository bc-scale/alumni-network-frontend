import { Button, Grid, Paper, Tooltip, Typography } from "@mui/material";
import React, { useEffect, useState } from "react";
import { IGroupRequest } from "../../api/group/groupTypes";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import {
	joinGroup,
	leaveGroup,
	selectUserGroups,
	selectUserId,
} from "../user/userSlice";
import {
	joinGroupThunk,
	leaveGroupThunk,
	selectActiveGroupId,
	setActiveGroupId,
} from "./groupSlice";

const Group = () => {
	const dispatch = useAppDispatch();

	const [responseMessage, setResponseMessage] = useState("");

	// Selectors
	const activeGroupId = useAppSelector(selectActiveGroupId);
	const group = useAppSelector(
		(state) =>
			state.group.groupById[activeGroupId ? parseInt(activeGroupId) : 0]
	);
	const userGroups = useAppSelector(selectUserGroups);
	const userId = useAppSelector(selectUserId);
	const isPartOfThisGroup =
		userGroups &&
		activeGroupId &&
		userGroups?.includes(parseInt(activeGroupId));

	/**
	 * Join a group click
	 * Sends a request to join group
	 * Updates state based on response
	 */
	const handleJoinGroupClick = () => {
		setResponseMessage("");
		if (activeGroupId) {
			const req = {
				userId,
				groupId: parseInt(activeGroupId),
			};
			dispatch(joinGroupThunk(req))
				.unwrap()
				.then(() => {
					dispatch(joinGroup(parseInt(activeGroupId)));
					setResponseMessage(
						"You just joined this group and now have access to its posts and events :)"
					);
				})
				.catch((error) => {
					console.log(error);
					setResponseMessage(
						"Error: Could not join this group. It might be private :("
					);
				});
		}
	};

	// cleanup to remove activeGroup on unmount
	useEffect(() => {
		return () => {
			dispatch(setActiveGroupId(undefined));
		};
	}, [dispatch]);

	// cleanup to remove responseMessage on unmount
	useEffect(() => {
		return () => {
			setResponseMessage("");
		};
	}, [activeGroupId]);

	/**
	 * Leave group click
	 * Sends a request to leave group
	 * Updates states based on response
	 */
	const handleLeaveGroupClick = () => {
		setResponseMessage("");
		if (activeGroupId) {
			const req: IGroupRequest = {
				userId,
				groupId: parseInt(activeGroupId),
			};
			dispatch(leaveGroupThunk(req))
				.unwrap()
				.then(() => {
					dispatch(leaveGroup(parseInt(activeGroupId)));
					setResponseMessage(
						"You left this group. Dont worry, you might be able to join again if you are lucky!"
					);
				})
				.catch((error) => {
					console.log(error);
					setResponseMessage(
						"Could not leave this group. God only knows why (-<^.^>-)"
					);
				});
		}
	};

	return (
		<Paper elevation={5} sx={{ padding: 2 }}>
			<Grid
				container
				direction="column"
				justifyContent="center"
				alignItems="center">
				<Typography>Name: {group.name}</Typography>
				<Typography>ID: {group.groupId}</Typography>
				<Typography>Description: {group.description}</Typography>
				<Typography>Users in this group: {group.alumnus?.length}</Typography>
				{/*{isPartOfThisGroup && (
					<Grid container>
						<Grid item>
							<TextField select></TextField>
						</Grid>
						<Grid>
							<Button>Invite user</Button>
						</Grid>
					</Grid>
				)}*/}
				<Grid container justifyContent="center" spacing={2}>
					<Grid item>
						<Tooltip
							title={
								isPartOfThisGroup
									? "You are already in this group"
									: "Join this group"
							}>
							<span>
								<Button
									variant="contained"
									color="primary"
									disabled={isPartOfThisGroup ? true : false}
									onClick={handleJoinGroupClick}>
									Join group
								</Button>
							</span>
						</Tooltip>
					</Grid>
					<Grid item>
						<Tooltip title={"Leave this group"}>
							<span>
								<Button
									variant="contained"
									color="error"
									disabled={!isPartOfThisGroup}
									onClick={handleLeaveGroupClick}>
									Leave group
								</Button>
							</span>
						</Tooltip>
					</Grid>
				</Grid>
			</Grid>
			{responseMessage && (
				<Typography
					sx={{ mt: 2 }}
					color={responseMessage.toLowerCase().includes("err") ? "red" : ""}>
					{responseMessage}
				</Typography>
			)}
		</Paper>
	);
};

export default Group;
