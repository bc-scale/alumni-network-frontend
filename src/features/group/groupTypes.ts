export interface IGroup {
	groupId: number;
	name: string;
	description: string;
	private: boolean;
	alumnus?: number[];
	events?: number[];
	posts?: number[];
}

export interface IGroupById {
	[id: number]: IGroup;
}
