import { Divider, Grid, List, ListSubheader, Popover } from "@mui/material";
import React from "react";
import { useAppSelector } from "../../app/hooks";
import GroupListItem from "./GroupListItem";
import { selectPublicAndPartOfGroups } from "./groupSlice";

export interface NavbarListProps {
	open: boolean;
	handleClose?: () => void;
	anchorEl: HTMLButtonElement | null;
}

/**
 * The groupList popover menu
 * @param props
 * @returns
 */
const GroupList = (props: NavbarListProps) => {
	const groupIds = useAppSelector(selectPublicAndPartOfGroups);

	return (
		<Popover
			open={props.open}
			onClose={props.handleClose}
			anchorEl={props.anchorEl}
			anchorOrigin={{
				vertical: "bottom",
				horizontal: "left",
			}}>
			<Grid
				container
				sx={{ backgroundColor: "" }}
				direction="column"
				padding={1}>
				<List subheader={<ListSubheader>Groups</ListSubheader>}>
					<Divider />
					{groupIds.length > 0
						? groupIds.map((id) => (
								<GroupListItem
									handleClose={props.handleClose}
									key={id}
									id={id}
								/>
						  ))
						: "No groups"}
				</List>
			</Grid>
		</Popover>
	);
};

export default GroupList;
