import { ListItem, ListItemButton, ListItemText } from "@mui/material";
import React from "react";
import { useNavigate } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { selectName, selectUserGroups } from "../user/userSlice";
import { selectActiveGroupId, setActiveGroupId } from "./groupSlice";
import CircleIcon from "@mui/icons-material/Circle";

export interface NavbarListItemProps {
	id: string;
	handleClose?: () => void;
}

/**
 * Each item in the groupList popover menu
 * @param props
 * @returns
 */
const GroupListItem = (props: NavbarListItemProps) => {
	const dispatch = useAppDispatch();
	const navigate = useNavigate();

	const activeGroupId = useAppSelector(selectActiveGroupId);
	const group = useAppSelector(
		(state) => state.group.groupById[parseInt(props.id)]
	);
	const userGroups = useAppSelector(selectUserGroups);
	const user = useAppSelector(selectName);

	/**
	 * Sets the active group id to the clicked groups id
	 */
	const handleGroupItemClick = () => {
		if (user !== "") {
			dispatch(setActiveGroupId(props.id));
			navigate("/group");
			props.handleClose && props.handleClose();
		} else alert("log in first");
	};
	return (
		<ListItem
			sx={{
				backgroundColor: activeGroupId === props.id ? "lavender" : "",
				borderRadius: 1,
				"&:hover": { bgcolor: "lavender" },
			}}
			disablePadding>
			<ListItemButton
				disabled={activeGroupId === props.id}
				onClick={handleGroupItemClick}>
				{userGroups?.includes(parseInt(props.id)) && (
					<CircleIcon color="success" />
				)}
				<ListItemText primary={group.name} />
			</ListItemButton>
		</ListItem>
	);
};

export default GroupListItem;
