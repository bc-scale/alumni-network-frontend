import { IGroupRequest } from "./../../api/group/groupTypes";
import {
	fetchGroups,
	joinGroupRequest,
	leaveGroupRequest,
} from "./../../api/group/groupApi";
import { IGroupById, IGroup } from "./groupTypes";
import { DataStatus } from "./../../common/commonTypes";
import { createAsyncThunk, createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../../app/store";

export const fetchAllGroups = createAsyncThunk(
	"group/fetchGroups",
	async (_, thunkAPI) => {
		const response = await fetchGroups();
		if (!response.success) {
			return thunkAPI.rejectWithValue({
				errorMessage: "Could not fetch groups",
			});
		}
		return response.data;
	}
);

export const joinGroupThunk = createAsyncThunk(
	"group/joinGroup",
	async (req: IGroupRequest, thunkAPI) => {
		const response = await joinGroupRequest(req);
		if (!response.success) {
			return thunkAPI.rejectWithValue({
				errorMessage: "Could not join group",
			});
		}
		return response.data;
	}
);

export const leaveGroupThunk = createAsyncThunk(
	"group/leaveGroup",
	async (req: IGroupRequest, thunkAPI) => {
		const response = await leaveGroupRequest(req);
		if (!response.success) {
			return thunkAPI.rejectWithValue({
				errorMessage: "Could not leave this group",
			});
		}
		return response.data;
	}
);

interface GroupState {
	groupStatus: DataStatus;
	groupIds: string[];
	groupById: IGroupById;
	activeGroup?: string;
}

const initialState: GroupState = {
	groupStatus: DataStatus.Idle,
	groupIds: [],
	groupById: {},
};

/**
 * Part of the redux store that handles storing all groups
 * Following the Redux normalizing pattern: {@link https://redux.js.org/usage/structuring-reducers/normalizing-state-shape#designing-a-normalized-state Redux normalizing state}
 */
export const groupSlice = createSlice({
	name: "group",
	initialState,
	reducers: {
		setActiveGroupId: (state, action: PayloadAction<string | undefined>) => {
			if (action.payload !== undefined)
				if (state.activeGroup === action.payload) state.activeGroup = undefined;
				else state.activeGroup = action.payload;
			else state.activeGroup = undefined;
		},
		setGroupStatus: (state, action: PayloadAction<DataStatus>) => {
			state.groupStatus = action.payload;
		},
	},
	extraReducers: (builder) => {
		builder.addCase(fetchAllGroups.fulfilled, (state, action) => {
			const byId = action.payload.reduce((byId: IGroupById, group: IGroup) => {
				byId[group.groupId] = group;
				return byId;
			}, {});
			state.groupById = byId;
			state.groupIds = Object.keys(byId);
			state.groupStatus = DataStatus.Succeeded;
		});
	},
});

export const { setActiveGroupId, setGroupStatus } = groupSlice.actions;

// Selectors
export const selectGroupIds = (state: RootState) => state.group.groupIds;
export const selectGroupById = (state: RootState) => state.group.groupById;
export const selectActiveGroupId = (state: RootState) =>
	state.group.activeGroup;

/**
 * Filters the groups to only return groups that are public and the user is part of
 * @param state redux store states
 * @returns a list of groups that are public or the user is part of
 */
export const selectPublicAndPartOfGroups = (state: RootState) => {
	let filteredGroupIds: string[] = [];
	const userGroupIds = state.user.groups;
	state.group.groupIds.forEach((id) => {
		const isPublic = state.group.groupById[parseInt(id)].private;
		const isPartOf = userGroupIds?.includes(parseInt(id));
		if (isPartOf || !isPublic) {
			filteredGroupIds.push(id);
		}
	});
	return filteredGroupIds;
};

export default groupSlice.reducer;
