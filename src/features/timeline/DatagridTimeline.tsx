import React from "react";
import { DataGrid, GridColDef, GridValueGetterParams } from "@mui/x-data-grid";
import { Grid } from "@mui/material";
import { posts } from "./postData";

const columns: GridColDef[] = [
	{ field: "title", headerName: "Title", width: 100 },
	{ field: "time", headerName: "Time", width: 100 },
	{ field: "createdBy", headerName: "User", width: 80 },
	{ field: "audience", headerName: "Audience", width: 100 },
	{ field: "topic", headerName: "Topic", width: 100 },
];

// not in use
const DatagridTimeline = () => {
	return (
		<Grid container sx={{ height: 400 }} justifyContent="center">
			<Grid sx={{ width: 800 }} item>
				<DataGrid rows={posts} columns={columns}></DataGrid>
			</Grid>
		</Grid>
	);
};

export default DatagridTimeline;
