import {
	Box,
	Button,
	Grid,
	Paper,
	Table,
	TableBody,
	TableCell,
	TableContainer,
	TableHead,
	TableRow,
	Tabs,
	Tooltip,
} from "@mui/material";
import React, { useEffect, useRef, useState } from "react";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { DataStatus } from "../../common/commonTypes";
import PostModal from "../post/PostModal";
import EventModal from "../event/EventModal";
import Tab from "@mui/material/Tab";
import TabContext from "@mui/lab/TabContext";
import TabList from "@mui/lab/TabList";
import TabPanel from "@mui/lab/TabPanel";
import {
	fetchAllPostsForLoggedInUser,
	selectFilteredAndSortedByLastUpdatedPostIds,
	selectPostById,
	selectPostsStatus,
	setActivePostId,
	setPostsStatus,
} from "../post/postSlice";
import { fetchAllUsers, selectUserStatus } from "../allUsers/usersSlice";
import {
	fetchAllEvents,
	selectEventStatus,
	selectFilteredEventIds,
	setActiveEventId,
	setEventStatus,
} from "../event/eventSlice";
import { fetchAllGroups } from "../group/groupSlice";
import { fetchAllTopics } from "../topic/topicSlice";
import TimelinePostItem from "./TimelinePostItem";
import TimelineEventItem from "./TimelineEventItem";
import { fetchAllRsvp } from "../rsvp/rsvpSlice";
import { selectUserId } from "../user/userSlice";
import TimelinePosts from "./TimelinePosts";
import TimelineEvents from "./TimelineEvents";
import PostAddIcon from "@mui/icons-material/PostAdd";
import EventIcon from "@mui/icons-material/Event";

interface IColumn {
	id: "title" | "lastUpdated" | "creator" | "content" | "topic";
	label: string;
	minWidth?: number;
	maxWidth?: number;
	align?: "right";
	format?: (value: number) => string;
}

export const columns: readonly IColumn[] = [
	{ id: "title", label: "Title", minWidth: 75 },
	{ id: "lastUpdated", label: "Updated", minWidth: 50 },
	{
		id: "creator",
		label: "Creator",
		minWidth: 75,
	},
	{
		id: "topic",
		label: "Topic",
		minWidth: 100,
	},
	{
		id: "content",
		label: "Content",
		minWidth: 100,
	},
];

/**
 * The timeline component (List of posts/events)
 * @returns timeline component
 */
const Timeline = () => {
	const dispatch = useAppDispatch();
	const [tabValue, setTabValue] = useState("1");

	const [openPost, setOpenPost] = useState(false);
	const [openEvent, setOpenEvent] = useState(false);

	const handleOpenPost = () => setOpenPost(true);
	const handleClosePost = () => setOpenPost(false);

	const handleOpenEvent = () => setOpenEvent(true);
	const handleCloseEvent = () => setOpenEvent(false);

	const postsStatus = useAppSelector(selectPostsStatus);
	const eventStatus = useAppSelector(selectEventStatus);
	const loggedInUserId = useAppSelector(selectUserId);
	const allUsersStatus = useAppSelector(selectUserStatus);

	const [loading, setLoading] = useState(false);
	const timer = useRef<any>();

	// Refetching data every 30s
	useEffect(() => {
		if (
			postsStatus === DataStatus.Idle &&
			eventStatus === DataStatus.Idle &&
			allUsersStatus === DataStatus.Succeeded &&
			!loading
		) {
			clearTimeout(timer.current);
			setLoading(true);
			dispatch(fetchAllPostsForLoggedInUser(loggedInUserId)).then(() => {
				dispatch(fetchAllEvents()).then(() => {
					setLoading(false);
					clearTimeout(timer.current);
					timer.current = setTimeout(() => {
						dispatch(setPostsStatus(DataStatus.Idle));
						dispatch(setEventStatus(DataStatus.Idle));
					}, 30000);
				});
			});
		}
	}, [
		postsStatus,
		eventStatus,
		dispatch,
		loggedInUserId,
		allUsersStatus,
		loading,
	]);

	const handleTabChange = (event: React.SyntheticEvent, newValue: string) => {
		setTabValue(newValue);
	};

	return (
		<Grid container>
			<Grid
				container
				justifyContent="center"
				sx={{
					flexDirection: "column",
					alignItems: "center",
				}}>
				<Paper elevation={5}>
					<TabContext value={tabValue}>
						<Grid sx={{ borderBottom: 1, borderColor: "divider" }}>
							<TabList
								variant="fullWidth"
								sx={{ alignSelf: "center" }}
								value={tabValue}
								onChange={handleTabChange}
								aria-label="tabs for timeline table">
								<Tab label="Posts" id="posts-tab" value="1" />
								<Tab label="Events" id="events-tab" value="2" />
							</TabList>
						</Grid>

						<TabPanel value="1" sx={{ p: 0 }}>
							<TimelinePosts />
						</TabPanel>
						<TabPanel value="2" sx={{ p: 0 }}>
							<TimelineEvents />
						</TabPanel>
					</TabContext>
				</Paper>
			</Grid>

			<PostModal openPost={openPost} handleClosePost={handleClosePost} />
			<EventModal openEvent={openEvent} handleCloseEvent={handleCloseEvent} />
			<Grid container justifyContent="center">
				<Tooltip arrow title="Create new post">
					<Button
						startIcon={<PostAddIcon />}
						variant="contained"
						sx={{ mt: 3, mb: 2 }}
						onClick={handleOpenPost}>
						Create Post
					</Button>
				</Tooltip>
				<Tooltip arrow title="Create new event">
					<Button
						startIcon={<EventIcon />}
						variant="contained"
						sx={{ mt: 3, mb: 2, ml: 2 }}
						onClick={handleOpenEvent}>
						Create Event
					</Button>
				</Tooltip>
			</Grid>
		</Grid>
	);
};

export default Timeline;
