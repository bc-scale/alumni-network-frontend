import {
	TableRow,
	TableCell,
	Typography,
	Tooltip,
	Button,
	ListItem,
} from "@mui/material";
import React from "react";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { DataStatus } from "../../common/commonTypes";
import { convertDate } from "../../common/helperFunctions";
import { selectUserStatus } from "../allUsers/usersSlice";
import { selectActivePostId } from "../post/postSlice";
import { selectTopicById, selectTopicStatus } from "../topic/topicSlice";

interface TimelineItemProps {
	id: string;
	handleTimelinePostItemClick: (id?: string) => void;
}

/**
 * Each post in the posts list in the timeline component
 * @param props
 * @returns A timeline post item
 */
const TimelinePostItem = (props: TimelineItemProps) => {
	const dispatch = useAppDispatch();

	const post = useAppSelector(
		(state) => state.posts.postById[parseInt(props.id)]
	);

	const user = useAppSelector(
		(state) => state.allUsers.userById[post.senderId]
	);

	const handleItemClick = () => {
		props.handleTimelinePostItemClick(props.id);
	};

	const userStatus = useAppSelector(selectUserStatus);
	const topicById = useAppSelector(selectTopicById);
	const activePost = useAppSelector(selectActivePostId);
	const topicStatus = useAppSelector(selectTopicStatus);

	return (
		<TableRow
			selected={props.id === activePost}
			onClick={handleItemClick}
			sx={{
				"&:hover": { cursor: "pointer", backgroundColor: "lavender" },
				"&.Mui-selected": { backgroundColor: "darkgrey" },
			}}
			tabIndex={-1}
			key={post.postId}>
			<Tooltip title="Title of the post">
				<TableCell>{post.title}</TableCell>
			</Tooltip>
			<Tooltip title={"Date and time: " + post.lastUpdated}>
				<TableCell>{convertDate(post.lastUpdated)}</TableCell>
			</Tooltip>
			<Tooltip title="Owner of the post/event">
				<TableCell>
					{userStatus === DataStatus.Succeeded && user.name}
				</TableCell>
			</Tooltip>
			<Tooltip title="topic">
				<TableCell>
					{topicStatus === DataStatus.Succeeded &&
					post.target_topics &&
					post.target_topics?.length > 0
						? post.target_topics.map((item) => topicById[item].name)
						: "No Topic"}
				</TableCell>
			</Tooltip>
			<TableCell>
				<Tooltip title={post.postMessage}>
					<Typography noWrap sx={{ maxWidth: 200 }}>
						{post.postMessage}
					</Typography>
				</Tooltip>
			</TableCell>
		</TableRow>
	);
};

export default TimelinePostItem;
