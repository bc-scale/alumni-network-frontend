// Old data. Not currently in use
const time = new Date().getTime();
const date = new Date(time).toLocaleString();

export const posts = [
	{
		id: 1,
		title: "Fest",
		time: date,
		createdBy: "Daniel",
		audience: "Party-Gutta",
		topic: "social",
	},
	{
		id: 2,
		title: "Museum",
		time: date,
		createdBy: "Robin",
		audience: "Group1",
		topic: "culture",
	},
	{
		id: 3,
		title: "Tur i parken",
		time: date,
		createdBy: "Alfred",
		audience: "Turgåerne",
		topic: "travel",
	},
	{
		id: 4,
		title: "Tur til toppen",
		time: date,
		createdBy: "Alfred",
		audience: "Turgåerne",
		topic: "travel",
	},
	{
		id: 5,
		title: "Besøke bestemor",
		time: date,
		createdBy: "Daniel",
		audience: "familie",
		topic: "social",
	},
	{
		id: 6,
		title: "Spise pizza",
		time: date,
		createdBy: "Alfred",
		audience: "familie",
		topic: "social",
	},
	{
		id: 7,
		title: "Presentasjon",
		time: date,
		createdBy: "Alfred",
		audience: "alumni network group 1",
		topic: "business",
	},
];
