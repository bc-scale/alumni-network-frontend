import {
	TableRow,
	TableCell,
	Typography,
	Tooltip,
	Button,
	ListItem,
} from "@mui/material";
import React from "react";
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { DataStatus } from "../../common/commonTypes";
import { convertDate } from "../../common/helperFunctions";
import { selectUserStatus } from "../allUsers/usersSlice";
import { selectActiveEvent } from "../event/eventSlice";
import { selectActivePostId } from "../post/postSlice";
import { selectTopicById, selectTopicStatus } from "../topic/topicSlice";

interface TimelineItemProps {
	id: string;
	handleTimelineEventItemClick: (id?: string) => void;
}

/**
 * Each event in the event list
 * @param props 
 * @returns a timeline event item
 */
const TimelineEventItem = (props: TimelineItemProps) => {
	const dispatch = useAppDispatch();

	const event = useAppSelector(
		(state) => state.event.eventById[parseInt(props.id)]
	);

	// get the user that created the event
	const user = useAppSelector(
		(state) => state.allUsers.userById[event.createdBy]
	);

	const handleItemClick = () => {
		props.handleTimelineEventItemClick(props.id);
	};

	const userStatus = useAppSelector(selectUserStatus);
	const topicById = useAppSelector(selectTopicById);
	const activeEvent = useAppSelector(selectActiveEvent);
	const topicStatus = useAppSelector(selectTopicStatus);

	return (
		<TableRow
			selected={props.id === activeEvent}
			onClick={handleItemClick}
			sx={{
				"&:hover": { cursor: "pointer", backgroundColor: "lavender" },
				"&.Mui-selected": { backgroundColor: "darkgrey" },
			}}
			tabIndex={-1}
			key={event.eventId}>
			<Tooltip title={event.title}>
				<TableCell>{event.title}</TableCell>
			</Tooltip>
			<Tooltip title={"Date and time: " + event.lastUpdated}>
				<TableCell>{convertDate(event.lastUpdated)}</TableCell>
			</Tooltip>
			<Tooltip title="Owner of the post/event">
				<TableCell>
					{userStatus === DataStatus.Succeeded && user.name}
				</TableCell>
			</Tooltip>
			<Tooltip title="topic">
				<TableCell>
					{topicStatus === DataStatus.Succeeded &&
					event.topics &&
					event.topics?.length > 0
						? event.topics.map((item) => topicById[item].name + " ")
						: "No Topic"}
				</TableCell>
			</Tooltip>
			<TableCell>
				<Tooltip title={event.description}>
					<Typography noWrap sx={{ maxWidth: 200 }}>
						{event.description}
					</Typography>
				</Tooltip>
			</TableCell>
		</TableRow>
	);
};

export default TimelineEventItem;
