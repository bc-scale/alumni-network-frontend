export interface IEventBody {
	title: string;
	description: string;
	createdBy: number;
	allowGuests: boolean;
	startTime?: Date | null;
	endTime?: Date | null;
	bannerImg?: string;
}

export interface IUpdateEvent {
	eventId: number;
	body: IEventBody;
}
