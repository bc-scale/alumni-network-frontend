export interface ITopic {}

export interface ITopicMembershipRequest {
	userId: number;
	topicId: number;
}
