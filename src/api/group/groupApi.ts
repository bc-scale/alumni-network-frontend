import { createAuthenticatedHeader, createHeader } from "../helperFunctions";
import { IGroup, IGroupRequest } from "./groupTypes";

const apiUrl = process.env.REACT_APP_ALUMNI_NETWORK_URL;

/**
 * Fetches all groups
 * @returns Groups
 */
export const fetchGroups = async () => {
	try {
		const response = await fetch(`${apiUrl}/group`, {
			method: "GET",
			headers: createAuthenticatedHeader(),
		});

		if (!response.ok) throw new Error("Could not fetch groups");

		const data = await response.json();
		return { success: true, data };
	} catch (error: any) {
		return error.message;
	}
};

/**
 * Fetches a group by id
 * @param id Id of the group
 * @returns a group
 */
export const fetchGroupById = async (id: number) => {
	try {
		const response = await fetch(`${apiUrl}/group/${id}`);

		if (!response.ok) throw new Error("Could not fetch group widh id " + id);

		const data = await response.json();
		return data;
	} catch (error: any) {
		return error.message;
	}
};

/**
 * Create a new group
 * @param group the group information
 * @returns the created group
 */
export const createGroup = async (group: IGroup) => {
	try {
		const response = await fetch(`${apiUrl}/group`, {
			method: "POST",
			headers: createAuthenticatedHeader(),
			body: JSON.stringify({
				group,
			}),
		});
		if (!response.ok)
			throw new Error(
				"Could not create new group with info: " + JSON.stringify(group)
			);

		const data = await response.json();
		return data;
	} catch (error: any) {
		return error.message;
	}
};

/**
 * Join a group request
 * @param req contains the group id and user id
 * @returns 
 */
export const joinGroupRequest = async (req: IGroupRequest) => {
	try {
		const response = await fetch(`${apiUrl}/group/${req.groupId}/join`, {
			method: "POST",
			headers: createAuthenticatedHeader(),
			body: JSON.stringify(req.userId),
		});
		if (!response.ok)
			throw new Error("Could not join the group. groupId: " + req.groupId);

		const data = await response.json();
		return { success: true, data };
	} catch (error: any) {
		return error.message;
	}
};

/**
 * Leave group request
 * @param req contains group id and user id
 * @returns 
 */
export const leaveGroupRequest = async (req: IGroupRequest) => {
	try {
		const response = await fetch(`${apiUrl}/group/${req.groupId}/leave`, {
			method: "DELETE",
			headers: createAuthenticatedHeader(),
			body: JSON.stringify(req.userId),
		});
		if (!response.ok) throw new Error("Could not delete group membership");
		const data = await response.json();
		return { success: true, data };
	} catch (error: any) {
		return error.message;
	}
};
