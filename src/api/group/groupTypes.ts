export interface IGroup {
	name: string;
	description: string;
	isPrivate: boolean;
}

export interface IGroupRequest {
	userId: number;
	groupId: number;
}
