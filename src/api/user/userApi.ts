import { createAuthenticatedHeader } from "./../helperFunctions";
import { createHeader } from "../helperFunctions";
import { IUpdateUser } from "./userTypes";

const apiUrl = process.env.REACT_APP_ALUMNI_NETWORK_URL;

// Not in use. Created early to work as sign in request
export const createUser = async (username: string, password: string) => {
	try {
		const response = await fetch(`${apiUrl}/user`, {
			method: "POST",
			headers: createAuthenticatedHeader(),
			body: JSON.stringify({
				username,
				password,
			}),
		});
		if (!response.ok)
			throw new Error("Could not create a new user with username " + username);

		const data = await response.json();
		return { success: true, data };
	} catch (error: any) {
		return error.message;
	}
};

/**
 * Request to login for a user. user info is in the token
 * @returns the user
 */
export const loginUser = async () => {
	try {
		const response = await fetch(`${apiUrl}/user/login`, {
			method: "GET",
			headers: createAuthenticatedHeader(),
		});

		if (response.status !== 303) throw new Error("No user found");

		const data = await response.json();

		return { success: true, data };
	} catch (error: any) {
		return error.message;
	}
};

/**
 * Fetches a user by id
 * @param id the id of the user
 * @returns the user
 */
export const getUserById = async (id: number) => {
	try {
		const response = await fetch(`${apiUrl}/user/${id}`, {
			method: "GET",
			headers: createAuthenticatedHeader(),
		});

		if (!response.ok) throw new Error("Could not fetch user with id " + id);

		const data = await response.json();
		return { success: true, data };
	} catch (error: any) {
		return { success: false, data: null };
	}
};

/**
 * Updates the user profile
 * @param req contains the user id and the information to be updated
 * @returns the updated user information
 */
export const updateUser = async (req: IUpdateUser) => {
	try {
		const response = await fetch(`${apiUrl}/user/${req.id}`, {
			method: "PATCH",
			headers: createAuthenticatedHeader(),
			body: JSON.stringify(req.body),
		});
		if (!response.ok) throw new Error("Could not update user with id ");

		const data = await response.json();
		return { success: true, data };
	} catch (error: any) {
		return error.message;
	}
};

/**
 * Fetches all (associated) users
 * @returns users
 */
export const fetchUsers = async () => {
	try {
		const response = await fetch(`${apiUrl}/user`, {
			method: "GET",
			headers: createAuthenticatedHeader(),
		});
		if (!response.ok) throw new Error("Could not fetch users");
		const data = await response.json();
		return { success: true, data };
	} catch (error: any) {
		return error.message;
	}
};
