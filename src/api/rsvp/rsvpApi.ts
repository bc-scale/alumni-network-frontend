import { IRsvp } from "../../features/rsvp/rsvpTypes";
import { createAuthenticatedHeader } from "../helperFunctions";

const apiUrl = process.env.REACT_APP_ALUMNI_NETWORK_URL;

export const fetchRsvp = async () => {
	try {
		const response = await fetch(`${apiUrl}/rsvp`, {
			method: "GET",
			headers: createAuthenticatedHeader(),
		});
		if (!response.ok) throw new Error("Could not fetch RSVPs");
		const data = await response.json();
		return { success: true, data };
	} catch (error: any) {
		return error.message;
	}
};

export const createRSVP = async (rsvp: IRsvp) => {
	try {
		const response = await fetch(`${apiUrl}/rsvp`, {
			method: "POST",
			headers: createAuthenticatedHeader(),
			body: JSON.stringify(rsvp),
		});
		if (!response.ok) throw new Error("Could not create RSVP");
		const data = await response.json();
		return { success: true, data };
	} catch (error: any) {
		return error.message;
	}
};
