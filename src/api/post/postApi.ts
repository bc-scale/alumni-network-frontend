import { IPost } from "../../features/post/postTypes";
import { createAuthenticatedHeader } from "../helperFunctions";
import { IPostBody } from "./postApiTypes";

const apiUrl = process.env.REACT_APP_ALUMNI_NETWORK_URL;

/**
 * Fetches all available posts
 * @returns posts
 */
export const fetchPosts = async () => {
	try {
		const response = await fetch(`${apiUrl}/post`, {
			method: "GET",
			headers: createAuthenticatedHeader(),
		});
		if (!response.ok) throw new Error("Could not fetch posts");
		const data = response.json();
		return { success: true, data };
	} catch (error: any) {
		return error.message;
	}
};

/**
 * Fetches all posts for a user id
 * @param userId the logged in user's id
 * @returns posts
 */
export const fetchPostsForUserId = async (userId: number) => {
	try {
		const response = await fetch(`${apiUrl}/post/user/${userId}`, {
			method: "GET",
			headers: createAuthenticatedHeader(),
		});
		if (!response.ok) throw new Error("Could not fetch posts for this user");
		const data = response.json();
		return { success: true, data };
	} catch (error: any) {
		return error.message;
	}
};

export const fetchPostById = async () => {
	try {
		const response = await fetch(`${apiUrl}/post`);
		if (!response.ok) throw new Error("Could not fetch this post");
	} catch (error: any) {
		return error.message;
	}
};

export const fetchPostsForUser = async (username: string) => {
	try {
		const response = await fetch(`${apiUrl}/post/${username}`);
		if (!response.ok)
			throw new Error("Could not fetch posts for this user: " + username);

		const data = response.json();
		return { success: true, data };
	} catch (error: any) {
		return error.message;
	}
};

export const fetchPostsToUserFromSpecificUser = async (
	user: string,
	userId: number
) => {
	try {
		const response = await fetch(`${apiUrl}/post/${user}/${userId}`);
		if (!response.ok)
			throw new Error(
				"Could not fetch the posts that was sent from user with id: " + userId
			);
	} catch (error: any) {
		return error.message;
	}
};

export const fetchPostsInGroup = async (groupId: number) => {
	try {
		const response = await fetch(`${apiUrl}/post/group/${groupId}`);
		if (!response.ok)
			throw new Error("Could not fetch posts in the group with id: " + groupId);
	} catch (error: any) {
		return error.message;
	}
};

export const fetchPostsInTopic = async (topicId: number) => {
	try {
		const response = await fetch(`${apiUrl}/post/topic/${topicId}`);
		if (!response.ok)
			throw new Error(
				"Could not fetch the posts in the topic with id: " + topicId
			);
	} catch (error: any) {
		return error.message;
	}
};

export const fetchPostsInEvent = async (eventId: number) => {
	try {
		const response = await fetch(`${apiUrl}/post/event/${eventId}`);
		if (!response.ok)
			throw new Error(
				"Could not fetch the posts in the event with id: " + eventId
			);
	} catch (error: any) {
		return error.message;
	}
};

/**
 * Create a new post
 * @param post The information in the post
 * @returns the created post
 */
export const createPostRequest = async (post: IPostBody) => {
	try {
		const response = await fetch(`${apiUrl}/post`, {
			method: "POST",
			headers: createAuthenticatedHeader(),
			body: JSON.stringify(post),
		});
		if (!response.ok) throw new Error("Could not create new post");
		const data = await response.json();
		return { success: true, data };
	} catch (error: any) {
		return error.message;
	}
};

export const updatePost = async (postId: number, post: IPost) => {
	try {
		const response = await fetch(`${apiUrl}/post/${postId}`, {
			method: "PUT",
			headers: createAuthenticatedHeader(),
			body: JSON.stringify({
				post,
			}),
		});
		if (!response.ok) throw new Error("Could not update post");
	} catch (error: any) {
		return error.message;
	}
};
