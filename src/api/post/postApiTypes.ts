export interface IPostBody {
	postMessage: string;
	title: string;
	lastUpdated: string;
	senderId: number;
	postTarget?: {
		postId: number;
	};
	target_user?: {
		userId: number;
	};
	target_group?: {
		groupId: number;
	};
	target_topics?: { topicId: number }[];
	target_event?: {
		eventId: number;
	};
}
