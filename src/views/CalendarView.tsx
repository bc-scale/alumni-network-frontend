import React from "react";
import UserCalendar from "../features/calendar/UserCalendar";
import withAuth from "../hoc/withAuth";

const CalendarView = () => {
	return (
		<div>
			<UserCalendar></UserCalendar>
		</div>
	);
};

export default withAuth(CalendarView);
