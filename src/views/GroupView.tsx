import { Grid } from "@mui/material";
import React from "react";
import { useAppSelector } from "../app/hooks";
import Group from "../features/group/Group";
import { selectActiveGroupId } from "../features/group/groupSlice";
import withAuth from "../hoc/withAuth";

const GroupView = () => {
	const activeGroupId = useAppSelector(selectActiveGroupId);
	return (
		<Grid container justifyContent="center">
			{activeGroupId ? (
				<Group />
			) : (
				"No active group. Select one from the group list"
			)}
		</Grid>
	);
};

export default withAuth(GroupView);
