import React, { useEffect, useState } from "react";
import {
	BrowserRouter,
	Navigate,
	Route,
	Routes,
	useNavigate,
} from "react-router-dom";
import LoginView from "./views/LoginView";
import UserSettingsView from "./views/UserSettingsView";
import CalendarView from "./views/CalendarView";
import TimelineView from "./views/TimelineView";
import ThreadView from "./views/ThreadView";
import GroupView from "./views/GroupView";
import UserProfileView from "./views/UserProfileView";
import Navbar from "./common/navbar/Navbar";
import { CssBaseline } from "@mui/material";
import { useAppDispatch, useAppSelector } from "./app/hooks";
import { addAuthToken, authenticationToken } from "./api/helperFunctions";
import { useAuth0 } from "@auth0/auth0-react";
import { setLoginMessage } from "./features/login/loginSlice";
import {
	fetchAllPosts,
	fetchAllPostsForLoggedInUser,
	selectPostsStatus,
	setPostsStatus,
} from "./features/post/postSlice";
import {
	fetchAllEvents,
	selectEventStatus,
	setEventStatus,
} from "./features/event/eventSlice";
import {
	fetchUserInfoWithToken,
	selectUserId,
} from "./features/user/userSlice";
import { DataStatus } from "./common/commonTypes";
import {
	fetchAllUsers,
	selectUserStatus,
} from "./features/allUsers/usersSlice";
import { fetchAllGroups } from "./features/group/groupSlice";
import { fetchAllRsvp } from "./features/rsvp/rsvpSlice";
import { fetchAllTopics } from "./features/topic/topicSlice";

function App() {
	const dispatch = useAppDispatch();

	const { getAccessTokenSilently, getIdTokenClaims, isAuthenticated } =
		useAuth0();
	const [message, setMessage] = useState("");

	const allUsersStatus = useAppSelector(selectUserStatus);
	const [loading, setLoading] = useState(false);

	/**
	 * Decode jwt
	 * @param token accessToken jwt
	 * @returns a decoded jwt parsed as json
	 */
	function parseJwt(token: string) {
		var base64Url = token.split(".")[1];
		var base64 = base64Url.replace(/-/g, "+").replace(/_/g, "/");
		var jsonPayload = decodeURIComponent(
			atob(base64)
				.split("")
				.map(function (c) {
					return "%" + ("00" + c.charCodeAt(0).toString(16)).slice(-2);
				})
				.join("")
		);
		return JSON.parse(jsonPayload);
	}

	useEffect(() => {
		if (isAuthenticated) {
			setMessage("Fetching token...");
			console.log("auth");
			(async () => {
				try {
					const token = await getAccessTokenSilently({
						audience: "https://alumni-network/auth",
						scope: "read:messages",
					});
					addAuthToken(token);
					console.log(parseJwt(token));
					console.log(authenticationToken);
				} catch (error: any) {
					console.log(error.message);
					setMessage("Error fetching token");
				}
			})().finally(() => {
				const tokenClaims = getIdTokenClaims();
				console.log(tokenClaims);
				setMessage("Token is now fetched");
				dispatch(fetchUserInfoWithToken());
			});
		}
	}, [dispatch, getAccessTokenSilently, getIdTokenClaims, isAuthenticated]);

	useEffect(() => {
		dispatch(setLoginMessage(message));
	}, [message, dispatch]);

	useEffect(() => {
		if (
			allUsersStatus === DataStatus.Idle &&
			isAuthenticated &&
			authenticationToken
		) {
			dispatch(fetchAllUsers());
			dispatch(fetchAllGroups());
			dispatch(fetchAllTopics());
			dispatch(fetchAllRsvp());
		}
	});

	return (
		<BrowserRouter>
			<CssBaseline />
			<>
				{isAuthenticated && <Navbar />}
				<Routes>
					<Route path="/" element={<LoginView />}></Route>
					<Route path="/settings" element={<UserSettingsView />}></Route>
					<Route path="/profile" element={<UserProfileView />}></Route>
					<Route path="/timeline" element={<TimelineView />}></Route>
					<Route path="/calendar" element={<CalendarView />}></Route>
					<Route path="/thread" element={<ThreadView />}></Route>
					<Route path="/group" element={<GroupView />}></Route>
				</Routes>
			</>
		</BrowserRouter>
	);
}

export default App;
