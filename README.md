### Alumni Network

This is the frontend application of the Alumni Network Case presented at Experis Academy Winter 2022.

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app), using the [Redux](https://redux.js.org/) and [Redux Toolkit](https://redux-toolkit.js.org/) template. The development of this project followed best practices from the Redux documentation.

The backend API can be found at: 
- https://gitlab.com/abinayasak/alumni-network

## Table of contents
1. [Setup](#setup)
2. [Heroku](#heroku)
3. [User manual](#user-manual)
4. [Prerequisites](#prerequisites)
5. [Contributors](#contributors)

## Setup

To run this project locally you will need to clone the project:

```
git clone https://gitlab.com/Snaxai/alumni-network-frontend.git
```


In the project directory, you can run:
### `npm install`

It is important to install all necessary dependecies first with the command 'npm install'

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

## Heroku

This project is hosted on heroku:
- https://alumni-network-frontend.herokuapp.com/

## User Manual
A user manual can be found in the user manual folder <br />
``` https://gitlab.com/Snaxai/alumni-network-frontend/-/blob/main/User%20manual/User%20manual.pdf ```

## Prerequisites
 - Node v16
 - Recommended: Visual Studio Code

## Contributors
- Jo Urdal - @jourdal
- Abinaya Abbi Sakthivel @abinayasak
- Ole Henrik Johansen - @OleHJoh
- Daniel Mossestad - @Snaxai
- Robin Svanor - @robin.svanor
